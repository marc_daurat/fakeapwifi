#!/bin/bash

# Project Byzantium: captive-portal.sh
# This script does the heavy lifting of IP tables manipulation under the
# captive portal's hood.  It should only be used by the captive portal daemon.

# Written by Sitwon and The Doctor.
# Copyright (C) 2013 Project Byzantium
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

IPTABLES=/sbin/iptables
ARP=/sbin/arp

NEW_USER_CHAIN="captive"
NEW_USER_MARK="42"

WIFI_IFACE="wlan1"
WIFI_ADDR="192.168.42.1"

PORTAL_IP="10.6.66.0"

# Set up the choice tree of options that can be passed to this script.
case "$1" in
    'initialize')
        # Initialize the IP tables ruleset by creating a new chain for captive
        # portal users.
        $IPTABLES -N $NEW_USER_CHAIN -t mangle

        # Convert the IP address of the client interface into a netblock.
        CLIENTNET=`echo $WIFI_ADDR | sed 's/1$/0\/24/'`

        # Exempt traffic which does not originate from the client network.
        $IPTABLES -t mangle -A PREROUTING -p all ! -s $CLIENTNET -j RETURN

        # Traffic not exempted by the above rules gets kicked to the captive
        # portal chain.  When a use clicks through a rule is inserted above
        # this one that matches them with a RETURN.
        $IPTABLES -t mangle -A PREROUTING -j $NEW_USER_CHAIN

        # Traffic not coming from an accepted user gets marked $NEW_USER_MARK.
        $IPTABLES -t mangle -A $NEW_USER_CHAIN -j MARK --set-mark $NEW_USER_MARK

        # Traffic which has been marked $NEW_USER_MARK and is headed for 80/TCP or 443/TCP
        # should be redirected to the captive portal web server.
        $IPTABLES -t nat -A PREROUTING -m mark --mark $NEW_USER_MARK -p tcp --dport 80 \
            -j DNAT --to-destination $PORTAL_IP:80
        $IPTABLES -t nat -A PREROUTING -m mark --mark $NEW_USER_MARK -p tcp --dport 443 \
            -j DNAT --to-destination $PORTAL_IP:443
        $IPTABLES -t nat -A PREROUTING -m mark --mark $NEW_USER_MARK -p udp --dport 53 \
            -j DNAT --to-destination $PORTAL_IP:53

        # HTTP replies come from the same port the requests were received by.
        # Rewrite the outbound packets to appear to come from the appropriate
        # ports.
#        $IPTABLES -t nat -A POSTROUTING -d $CLIENTNET -p tcp --sport 31337 -j SNAT --to-source :80
#        $IPTABLES -t nat -A POSTROUTING -d $CLIENTNET -p tcp --sport 31338 -j SNAT --to-source :443

        # Replies from fake_dns.py come from the same port because they're
        # UDP.  Rewrite the packet headers so it loos like it's from port
        # 53/udp.
        $IPTABLES -t nat -A POSTROUTING -d $CLIENTNET -p udp --sport 31339 \
            -j SNAT --to-source :53

        # All other traffic which is marked $NEW_USER_MARK is just dropped
        $IPTABLES -t filter -A FORWARD -m mark --mark $NEW_USER_MARK -j DROP

        # Allow incoming traffic that is headed for the local node.
        $IPTABLES -t filter -A INPUT -p tcp --dport 53 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p tcp --dport 80 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p tcp --dport 443 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p tcp --dport 1248 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p tcp --dport 6667 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p tcp --dport 8089 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p tcp --dport 9001 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p tcp --dport 9005 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p tcp --dport 9006 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p tcp --dport 9090 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p tcp --dport 31337 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p tcp --dport 31338 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p udp --dport 53 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p udp --dport 67 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p udp --dport 698 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p udp --dport 5353 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p udp --dport 31339 -j ACCEPT
        $IPTABLES -t filter -A INPUT -p icmp -j ACCEPT

        # But reject anything else coming from unrecognized users.
        $IPTABLES -t filter -A INPUT -m mark --mark $NEW_USER_MARK -j DROP

        exit 0
        ;;
    'add')
        # $2: IP address of client.
        CLIENT=$2

        # Isolate the MAC address of the client in question.
        CLIENTMAC=`$ARP -n | grep ':' | grep $CLIENT | awk '{print $3}'`

        # Add the MAC address of the client to the whitelist, so it'll be able
        # to access the mesh even if its IP address changes.
        $IPTABLES -t mangle -I $NEW_USER_CHAIN -m mac --mac-source \
            $CLIENTMAC -j RETURN

        exit 0
        ;;
    'remove')
        # $2: IP address of client.
        CLIENT=$2

        # Isolate the MAC address of the client in question.
        CLIENTMAC=`$ARP -n | grep ':' | grep $CLIENT | awk '{print $3}'`

        # Delete the MAC address of the client from the whitelist.
        $IPTABLES -t mangle -D $NEW_USER_CHAIN -m mac --mac-source \
            $CLIENTMAC -j RETURN

        exit 0
        ;;
    'purge')
        # Purge all of the IP tables rules.
        $IPTABLES -F
        $IPTABLES -X
        $IPTABLES -t nat -F
        $IPTABLES -t nat -X
        $IPTABLES -t mangle -F
        $IPTABLES -t mangle -X
        $IPTABLES -t filter -F
        $IPTABLES -t filter -X

        exit 0
        ;;
    'list')
        # Display the currently running IP tables ruleset.
        $IPTABLES --list -n
        $IPTABLES --list -t nat -n
        $IPTABLES --list -t mangle -n
        $IPTABLES --list -t filter -n

        exit 0
        ;;
    *)
        echo "USAGE: $0 {initialize <IP> <interface>|add <IP> <interface>|remove <IP> <interface>|purge|list}"
        exit 0
    esac
