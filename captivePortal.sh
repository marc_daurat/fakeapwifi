#/bin/bash

# From http://www.andybev.com/index.php/Using_iptables_and_PHP_to_create_a_captive_portal

IPTABLES="/sbin/iptables"
NEW_USER_CHAIN="connected"
NEW_USER_MARK="42"

CLIENTNET="192.168.42.0/24"
PORTAL_IP="10.6.66.0"
PORTAL_PORT="80"

WIFI_IFACE="wlan1"
OUT_IFACE="eth0"


# Exempt traffic which does not originate from the client network.
#$IPTABLES -t mangle -A PREROUTING -p all ! -s $CLIENTNET -j RETURN

# This chain is for the $NEW_USER_CHAIN users
$IPTABLES -N $NEW_USER_CHAIN -t mangle

# All of the traffic is for this chain. Unauthorized packets will be drop soon
$IPTABLES -t mangle -A PREROUTING -j $NEW_USER_CHAIN -i $WIFI_IFACE

# DHCP
$IPTABLES -A INPUT -i $WIFI_IFACE -p udp -m udp --sport bootpc --dport bootps -j ACCEPT
# DNS
$IPTABLES -A INPUT -i $WIFI_IFACE -p udp -m udp --dport 53 -j ACCEPT

# New users are marked
$IPTABLES -t mangle -A $NEW_USER_CHAIN -j MARK --set-mark $NEW_USER_MARK

# All new users are redirected to the portal
$IPTABLES -t nat -A PREROUTING -m mark --mark $NEW_USER_MARK -p tcp --dport $PORTAL_PORT -j DNAT --to-destination $PORTAL_IP

# If there is any other marked packets, drop them 
#$IPTABLES -t filter -A FORWARD -m mark --mark $NEW_USER_MARK -j DROP
#$IPTABLES -t filter -A INPUT -m mark --mark $NEW_USER_MARK -j DROP

$IPTABLES -A POSTROUTING -t nat -o $OUT_IFACE -j MASQUERADE
