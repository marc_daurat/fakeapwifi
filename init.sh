#!/bin/bash

if [ `whoami` != "root" ] ; then
	echo "You need to be root !"
	exit 1
fi

IPTABLES="/sbin/iptables"

WIFI_IFACE="wlan1"

OUT_IFACE="eth0"

GATEWAY_IP="192.168.42.1/24"
NETWORK="192.168.42.0/24"

FAKE_HOSTS_IFACE="lo"
FAKE_HOSTS_FILE="/etc/dnsmasq.hosts"

# From http://stackoverflow.com/questions/14928573/sed-how-to-extract-ip-address-using-sed 
fake_hosts=`grep -oE '((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])' $FAKE_HOSTS_FILE`

for ip in $fake_hosts ; do
	ERR=`ip addr add $ip/32 dev $FAKE_HOSTS_IFACE > /dev/null`
done

echo 1 > /proc/sys/net/ipv4/ip_forward

ERR=`ip addr add $GATEWAY_IP dev $WIFI_IFACE > /dev/null`

$IPTABLES -A POSTROUTING -t nat -o $OUT_IFACE -j MASQUERADE

echo "Start DNS / DHCP and other services then :"

echo "./launch.sh"
