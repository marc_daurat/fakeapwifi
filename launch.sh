#!/bin/bash

if [ `whoami` != "root" ] ; then
	echo "You need to be root !"
	exit 0
fi

HOSTAPD_CONF_FILE="./hostapd.conf"
RESOLV_NEW="./resolv.conf"

cat $RESOLV_NEW > /etc/resolv.conf

#service dnsmasq restart

#service apache2 restart

hostapd $HOSTAPD_CONF_FILE


