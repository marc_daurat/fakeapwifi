#!/usr/bin/python

# Adapted from http://www.andybev.com/index.php/Using_iptables_and_PHP_to_create_a_captive_portal

import cgi,os;


USER_RECORD="../../loginPass.txt"
USER_CURRENT="/tmp/loginPass.txt"

ARP_REQUEST="/usr/sbin/arp -a"
MAC_FILTER="/bin/grep -oE '([0-9a-f]{2}:){5}[0-9a-f]{2}'"

IPTABLES_RULE = "sudo /sbin/iptables -I connected 1 -t mangle -m mac --mac-source {} -j RETURN"

test = open("/tmp/test.txt", 'a')

test.write("Salut gros")

def isKnow(name, password):
	know = open(USER_RECORD, 'r')

	for line in know:
		n = line.split(' ')[0]
		pwd = line.split(' ')[1]

		if n == name and pwd == password:
			return True

	know.close()

	return False

def isConnected(name, password):
	connected = open(USER_CURRENT, 'r')

	for line in connected:
		n = line.split(' ')[0]
		pwd = line.split(' ')[1]

		if n == name and pwd == password:
			return True

	connected.close()

	return False

def addHimToFile(name, password, fname):
	record = open(fname, 'a')

	record.write(name + ' ' + password)

	record.close()

def openCaptivePortal(ipAddr):

	# ARP request
	macAddr = ARP_REQUEST + ' ' + ipAddr + ' | ' + MAC_FILTER
	macAddr = subprocess.call(macAddr, shell=True)

	iptables = IPTABLES_RULE.format(macAddr)
	iptables = subprocess.call(iptables, shell=True)


form = cgi.FieldStorage()

if form.has_key("name"):
	name = form["name"].value

if form.has_key("password"):
	pwd = form["password"].value


if !isset(pwd) && !isset(name):
	return 1


remoteIP = os.environ['REMOTE_ADDR']

if not isKnow(name, pwd):
	addHimToFile(name, pwd, USER_RECORD):

if not isConnected(name, pwd):
	addHimToFile(name, pwd, USER_CURRENT):

	openCaptivePortal(remoteIP)

test.close()
